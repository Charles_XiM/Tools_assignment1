### **Edit history(Reason of change)**

#### 1st edit
>##### edit the CashRegister.java. Corrected many spell errors.
#### 2nd edit
>##### edit the CashRegister.java. Corrected the algorithm, add the symbol '$'.
#### 3rd edit
>##### edit the Transaction.java. Fixed the value bug.
#### 4th edit
>##### Merge branch 'milstone1_fixBug' into 'master'.
#### 5th edit
>##### edit the CashRegister.java. Add loops, receipt. Many details have been added.
#### 6th edit
>##### edit the CashRegister.java. Remove the display of the cash register balance after each transaction and only display at the end when the user quits.
#### 7th edit
>##### Merge branch 'milstone2_addingFeatures' into 'master'
#### 8th edit
>##### edit the CashRegister.java. translate to Chinese Version.
#### 9th edit
>##### Merge branch 'milstone2_addingFeatures(ChineseVersion)' into 'master'.
#### 10th edit
>##### edit the CashRegister.java. Added a new function to prevent input errors and change to English version.
#### 11th edit
>##### Merge branch 'milstone3_newFeature' into 'master'.
#### 12th edit
>##### edit the CashRegister.java. Add new features: add time data.
#### 13th edit
>##### edit the Transaction.java. Add time function.
#### 14th edit
>##### Merge branch 'milstone3_newFeature' into 'master'.
#### 15th edit
>##### add changes.md file.
#### 16th edit
>##### add user manual.md file.

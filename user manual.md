### **The user manual**
>##### >_This page will guide beginners on how to use the system. It will explain how to operate._
#### This system is designed for cashiers, it will help cashiers to reduce the pressure of work. 

#### Way to use it
>##### 1.git clone from github, 2.Copy the code into the compiler，3.Download the zip file, 4.Use git clone source code locally

#### Welcome To use
>##### system: Display: "Welcome to Charles market!   I will get your money :)".
>##### user:   Do not need to enter anything.
#### Preparation before formal use
>##### system: Display: "Please enter cash register's float: $".
>##### user:   Enter existing amount.
#### Formal use
>##### system: Display: "Do you want use it? (y|n)".
>##### user:   If you want to use, press 'y', else, press 'n'.
>###### _if user enter 'n', the system will return to the initial interface._

>##### system: If user enter invalid word, display: "You have typed wrong, Please reEnter.  Do you want to make a new transaction? (y|n)".
>##### user:   If you want to reenter, press 'y', else, press 'n'.
>###### _if user enter 'n', the system will keep show this infomation._

>##### system: Display: "Please enter the item's name:".
>##### user:   Enter the item name.
>##### ststem: Display: "Please enter the item's cost: $".
>##### user:   Enter the item price.

>##### system: Display: "Is all items have been entered? (y|n)".
>##### user:   Press 'n' to keep adding item, else, jump to charging interface.
>##### system: If user enters invalid word, display: "You have typed wrong, Please reEnter.  Do you want to make a new transaction? (y|n)".
>##### user:   If you want to reenter, press 'y', else, press 'n'.
>###### _if user enters 'n', the system will display: "Let' s buying something else.".

>##### system: Show what you bought.
>##### system: Display: "Please enter the cash amount tendered: $".
>##### user:   Enter the money you got.
>##### system: Display: "Amount of change required = $ ".
#### Show the receipt
>##### system: Show the item detials, include the name, price, cost, date.
>##### system: Show the information about: total cost, received money, charge.
>##### system: Display: "******Have a good day!******".
#### At the end of work
>##### system: Show the balance.

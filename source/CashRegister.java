import java.util.Scanner;

public class CashRegister
{
   public static void main(String[] args)
   {
      String s, c;
      double balance;
      boolean leave = false;
      String choice;
      String itemName;
      double spend;
      double needToPay = 0;
      double totalCost = 0;
      double cash;
      double charge;
      double cash;

      
      ArrayList<Transaction> shopCart = new ArrayList<>();
      DateTimeFormatter dataTime = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
      Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to Charles market!");
		System.out.println("I will get your money :)");

      System.out.print("Please enter cash register's float: $");
      s = scanner.nextLine();
      balance = Double.parseDouble(s);

      while(!leave) {
    	  System.out.print("Do you want use it again? (y|n) ");
    	  choice = scanner.nextLine();
    	  while(!choice.equalsIgnoreCase("y") && !choice.equalsIgnoreCase("n")) {
			    System.out.println("You have typed wrong, Please reEnter."); 
			    System.out.print("Do you want to make a new transaction? (y|n) ");
			    choice = scanner.nextLine();
		    }
    	  
    	  boolean discount = false;
    	  boolean finish = false;
    	  if(choice.equalsIgnoreCase("y")) {
    		  while(!finish) {
    			  System.out.print("Please enter the item's name: ");
    	          s = scanner.nextLine();
    	          itemName = s;

    	          System.out.print("Please enter the item's cost: $");
    	          c = scanner.nextLine();
    	          spend = Double.parseDouble(c);

    	          Transaction trans = new Transaction(s, Double.parseDouble(c));
    	          
    	          boolean end = false;
    	          for(Transaction t : shopCart) {
    	        	  if(t.getName().equals(trans.getName())) {
    	        		  if (t.getCost() == trans.getCost()) {
    	        			  end = true;
    	        		  }
    	        	  }
    	          }
    	          if(!end) {
    	        	  shopCart.add(trans);
    	        	  needToPay += trans.getCost();
    	        	  totalCost += trans.getCost();
    	          }
    	          else {
    	        	  System.out.println("You finished");
    	          }

    	          System.out.print("Is all items have been entered? (y|n) ");
    	          choice = scanner.nextLine();
    	          
    	          while(!choice.equalsIgnoreCase("y") && !choice.equalsIgnoreCase("n")) {
						System.out.println("You have typed wrong, Please reEnter."); 
						System.out.print("Is all items have been entered? (y|n) ");
						choice = scanner.nextLine();
					}
			
    	          if (choice.equalsIgnoreCase("y") ){
						leave = true;
    	          }
    	          else if (choice.equalsIgnoreCase("n")){
						System.out.println("Let' s buying something else.");
    	          }
     
    		  }
    		  boolean enough = false;
    		  while(!enough) {
    			  if(choice.equals("y")) {
    				  System.out.println("You selected " + shopCart.size() + " items, the amount you need to pay is: "
								+ needToPay);
    			  }
    			  int paid = 0;
    			  System.out.print("Please enter the cash amount tendered: $");
	              s = scanner.nextLine();
	              paid += Double.parseDouble(s);
	              cash += paid;
	              
	              balance += cash;
	              charge = cash - totalCost;
	              //c = Double.toString(Double.parseDouble(s) - trans.getCost());
	              System.out.println("Amount of change required = $" + charge);

	              
    		  }
    		  System.out.println("***********Receipt***********");
				int num = 1;
				//show all the item in the basket
				for (Transaction t: shopCart){
					System.out.println( num + ". Name: *" + t.getName() + "*  Price: *" + t.getCost() + "*" + "    Date: *" + dataTime.format(t.getDate())+ "*");
					num ++;
				}
				//payment info
				System.out.println("\nTotal Cost: " + totalCost);
				System.out.println("You have paid: " + cash);
				System.out.println("Change: " + charge);
				System.out.println("******Have a good day!******");
				shopCart.clear();
    	  }
    	  
    	  
      }
    //c = Double.toString(balance - trans.getCost() + Double.parseDouble(s));
      System.out.println("Balance of the Cash Register:" + balance);
   }
}
